/*
 * Copyright (C) 2004-2020 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package handlers.effecthandlers.consume;

import java.util.Arrays;

import com.l2jserver.gameserver.model.StatsSet;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.agathion.Agathion;
import com.l2jserver.gameserver.model.agathion.AgathionRepository;
import com.l2jserver.gameserver.model.conditions.Condition;
import com.l2jserver.gameserver.model.effects.AbstractEffect;
import com.l2jserver.gameserver.model.itemcontainer.Inventory;
import com.l2jserver.gameserver.model.items.instance.L2ItemInstance;
import com.l2jserver.gameserver.model.skills.BuffInfo;
import com.l2jserver.gameserver.network.serverpackets.ExBR_AgathionEnergyInfo;

/**
 * Consume AgathionEnergy effect implementation.
 * @author Zoey76
 */
public final class ConsumeAgathionEnergy extends AbstractEffect
{
	private final int _energy;
	
	public ConsumeAgathionEnergy(Condition attachCond, Condition applyCond, StatsSet set, StatsSet params)
	{
		super(attachCond, applyCond, set, params);
		_energy = params.getInt("energy", 0);
		setTicks(params.getInt("ticks"));
	}
	
	@Override
	public boolean onActionTime(BuffInfo info)
	{
		if (info.getEffected().isDead())
		{
			return false;
		}
		
		if (!info.getEffected().isPlayer())
		{
			return false;
		}
		
		final L2PcInstance target = info.getEffected().getActingPlayer();
		final Agathion agathionInfo = AgathionRepository.getInstance().getByNpcId(target.getAgathionId());
		if ((agathionInfo == null) || (agathionInfo.getMaxEnergy() <= 0))
		{
			return false;
		}
		
		final L2ItemInstance agathionItem = target.getInventory().getPaperdollItem(Inventory.PAPERDOLL_LBRACELET);
		if ((agathionItem == null) || (agathionInfo.getItemId() != agathionItem.getId()))
		{
			return false;
		}
		
		final int consumed = (int) (_energy * getTicksMultiplier());
		if ((consumed < 0) && ((agathionItem.getAgathionRemainingEnergy() + consumed) <= 0))
		{
			return false;
		}
		agathionItem.setAgathionRemainingEnergy(agathionItem.getAgathionRemainingEnergy() + consumed);
		
		// If item is agathion with energy, then send info to client.
		info.getEffected().sendPacket(new ExBR_AgathionEnergyInfo(Arrays.asList(agathionItem)));
		
		return true;
	}
}