/*
 * Copyright (C) 2004-2020 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.dao.impl.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.l2jserver.commons.database.ConnectionFactory;
import com.l2jserver.gameserver.dao.SiegeClanHallDAO;

/**
 * SiegeClanHall DAO MySQL implementation.
 * @author Мо3олЬ
 */
public class SiegeClanHallDAOMySQLImpl implements SiegeClanHallDAO
{
	private static final Logger LOG = LoggerFactory.getLogger(SiegeClanHallDAOMySQLImpl.class);
	
	private static final String DELETE = "DELETE FROM rainbowsprings_attacker_list WHERE clanId = ?";
	private static final String INSERT = "INSERT INTO rainbowsprings_attacker_list VALUES (?,?)";
	
	@Override
	public void removeAttacker(int clanId)
	{
		try (Connection con = ConnectionFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement(DELETE))
		{
			ps.setInt(1, clanId);
			ps.execute();
		}
		catch (Exception e)
		{
			LOG.warn("Could not to remove attacker clan ID {} from database!", clanId, e);
		}
	}
	
	@Override
	public void addAttacker(int clanId, long count)
	{
		try (Connection con = ConnectionFactory.getInstance().getConnection();
			PreparedStatement ps = con.prepareStatement(INSERT))
		{
			ps.setInt(1, clanId);
			ps.setLong(2, count);
			ps.execute();
		}
		catch (Exception e)
		{
			LOG.warn("Could not add attakers for clan ID {} and count {}!", clanId, count, e);
		}
	}
}
