/*
 * Copyright (C) 2004-2020 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.network.serverpackets;

import java.util.LinkedList;
import java.util.List;

import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.agathion.Agathion;
import com.l2jserver.gameserver.model.agathion.AgathionRepository;
import com.l2jserver.gameserver.model.items.instance.L2ItemInstance;

/**
 * Item List server packet.
 * @author Zoey76
 */
public final class ItemList extends AbstractItemPacket
{
	private final L2PcInstance _player;
	private final List<L2ItemInstance> _items = new LinkedList<>();
	private final List<L2ItemInstance> _agathions = new LinkedList<>();
	
	private final boolean _showWindow;
	
	public ItemList(L2PcInstance player, boolean showWindow)
	{
		_player = player;
		_showWindow = showWindow;
		
		for (L2ItemInstance item : player.getInventory().getItems())
		{
			if (!item.isQuestItem())
			{
				_items.add(item);
			}
			
			final Agathion agathion = AgathionRepository.getInstance().getByItemId(item.getId());
			if ((agathion != null) && (agathion.getMaxEnergy() > 0))
			{
				_agathions.add(item);
			}
		}
	}
	
	@Override
	protected final void writeImpl()
	{
		writeC(0x11);
		writeH(_showWindow ? 0x01 : 0x00);
		writeH(_items.size());
		_items.forEach(this::writeItem);
		writeInventoryBlock(_player.getInventory());
		if (!_agathions.isEmpty())
		{
			_player.sendPacket(new ExBR_AgathionEnergyInfo(_agathions));
		}
	}
	
	@Override
	public void runImpl()
	{
		getClient().sendPacket(new ExQuestItemList(_player));
	}
}
