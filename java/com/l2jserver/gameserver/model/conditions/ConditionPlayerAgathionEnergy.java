/*
 * Copyright (C) 2004-2020 L2jEnergy Server
 * 
 * This file is part of L2jEnergy Server.
 * 
 * L2jEnergy Server is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * L2jEnergy Server is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */
package com.l2jserver.gameserver.model.conditions;

import com.l2jserver.gameserver.model.actor.L2Character;
import com.l2jserver.gameserver.model.actor.instance.L2PcInstance;
import com.l2jserver.gameserver.model.agathion.Agathion;
import com.l2jserver.gameserver.model.agathion.AgathionRepository;
import com.l2jserver.gameserver.model.itemcontainer.Inventory;
import com.l2jserver.gameserver.model.items.L2Item;
import com.l2jserver.gameserver.model.items.instance.L2ItemInstance;
import com.l2jserver.gameserver.model.skills.Skill;

/**
 * Condition agathion energy.
 * @author Zoey76
 */
public class ConditionPlayerAgathionEnergy extends Condition
{
	
	private final int _energy;
	
	public ConditionPlayerAgathionEnergy(int energy)
	{
		_energy = energy;
	}
	
	@Override
	public boolean testImpl(L2Character effector, L2Character effected, Skill skill, L2Item item)
	{
		if (!effector.isPlayer())
		{
			return false;
		}
		
		final L2PcInstance player = effector.getActingPlayer();
		final Agathion agathionInfo = AgathionRepository.getInstance().getByNpcId(player.getAgathionId());
		if ((agathionInfo == null) || (agathionInfo.getMaxEnergy() <= 0))
		{
			return false;
		}
		
		final L2ItemInstance agathionItem = player.getInventory().getPaperdollItem(Inventory.PAPERDOLL_LBRACELET);
		if ((agathionItem == null) || (agathionInfo.getItemId() != agathionItem.getId()))
		{
			return false;
		}
		return agathionItem.getAgathionRemainingEnergy() >= _energy;
	}
}